import numpy as np;
import copy;
import time;
import sys;
import subprocess, platform

DIM = 6;            # Board dimension (DO NOT TOUCH)
T_LIMIT = 30;      # Recursive search limit

class Tile:
    def __init__(self, type, visited):
        self.type = type;
        self.visited = visited;

def find_sides(i, j, dimension):
    sides = [];

    x = i;
    y = j;
    while(x != 0):
        x -= 1;
    if not((i, j) == (x, y)):
        sides.append((x, y));

    x = i;
    y = j;
    while(y != dimension - 1):
        y += 1;
    if not((i, j) == (x, y)):
        sides.append((x, y));

    x = i;
    y = j;
    while(x != dimension - 1):
        x += 1;
    if not((i, j) == (x, y)):
        sides.append((x, y));

    x = i;
    y = j;
    while(y != 0):
        y -= 1;
    if not((i, j) == (x, y)):
        sides.append((x, y));

    return sides;

def find_corners(i, j, dimension):
    corners = [];

    x = i;
    y = j;
    while(x != 0 and y != 0):
        x -= 1;
        y -= 1;
    if not((i, j) == (x, y)):
        corners.append((x,y));

    x = i;
    y = j;
    while(x != 0 and y != dimension - 1):
        x -= 1;
        y += 1;
    if not((i, j) == (x, y)):
        corners.append((x,y));

    x = i;
    y = j;
    while(x != dimension - 1 and  y != dimension - 1):
        x += 1;
        y += 1;
    if not((i, j) == (x, y)):
        corners.append((x,y));

    x = i;
    y = j;
    while(x != dimension - 1 and y != 0):
        x += 1;
        y -= 1;
    if not((i, j) == (x, y)):
        corners.append((x,y));

    return corners

def solve_tile(i, j, cp, lp, c):
#    print("visiting (%d, %d)" %(i, j));
#    print("cp:")
#    print(cp)
#    print("lp:")
#    print(lp)
    if (time.perf_counter() - c) > T_LIMIT:
        return cp, lp;
    c += 1;
    if board[i][j].visited == True:
#        print("returning");
        return cp, lp;
    else:
#        print("setting (%d, %d) as visited" %(i, j));
        board[i][j].visited = True;
#        print("appending (%d, %d)" %(i, j));
        cp.append((i, j));
        if len(cp) > len(lp):
            lp = copy.deepcopy(cp);
        if len(lp) == DIM * DIM:
            return cp, lp;
            sys.exit(0);
        print("Longest path: %d" %len(lp));
        print(lp);


    if board[i][j].type == "1":
 #       print("type 1 entered");
        if i == 0:                      # If in top row
            if j == 0:
                cp, lp = solve_tile(0, 1, cp, lp, c);
                cp, lp = solve_tile(1, 0, cp, lp, c);
                cp, lp = solve_tile(1, 1, cp, lp, c);
            elif j == DIM - 1:
                cp, lp = solve_tile(0, j - 1, cp, lp, c);
                cp, lp = solve_tile(1, j - 1, cp, lp, c);
                cp, lp = solve_tile(1, j, cp, lp, c);
            else:
                cp, lp = solve_tile(0, j - 1, cp, lp, c);
                cp, lp = solve_tile(0, j + 1, cp, lp, c);
                cp, lp = solve_tile(1, j - 1, cp, lp, c);
                cp, lp = solve_tile(1, j, cp, lp, c);
                cp, lp = solve_tile(1, j + 1, cp, lp, c);
        elif i == DIM - 1:        # If in bottom row
            if j == 0:
                cp, lp = solve_tile(i - 1, j, cp, lp, c);
                cp, lp = solve_tile(i - 1, j + 1, cp, lp, c);
                cp, lp = solve_tile(i, j + 1, cp, lp, c);
            elif j == DIM - 1:
                cp, lp = solve_tile(i, j - 1, cp, lp, c);
                cp, lp = solve_tile(i - 1, j - 1, cp, lp, c);
                cp, lp = solve_tile(i - 1, j, cp, lp, c);
            else:
                cp, lp = solve_tile(i, j - 1, cp, lp, c);
                cp, lp = solve_tile(i - 1, j - 1, cp, lp, c);
                cp, lp = solve_tile(i - 1, j, cp, lp, c);
                cp, lp = solve_tile(i - 1, j + 1, cp, lp, c);
                cp, lp =solve_tile(i, j + 1, cp, lp, c);
        else:                           # If in middle rows
            if j == 0:
                cp, lp = solve_tile(i - 1, j, cp, lp, c);
                cp, lp = solve_tile(i - 1, j + 1, cp, lp, c);
                cp, lp = solve_tile(i, j + 1, cp, lp, c);
                cp, lp = solve_tile(i + 1, j + 1, cp, lp, c);
                cp, lp = solve_tile(i + 1, j, cp, lp, c);
            elif j == DIM - 1:
                cp, lp = solve_tile(i - 1, j, cp, lp, c);
                cp, lp = solve_tile(i - 1, j - 1, cp, lp, c);
                cp, lp = solve_tile(i, j - 1, cp, lp, c);
                cp, lp = solve_tile(i + 1, j - 1, cp, lp, c);
                cp, lp = solve_tile(i + 1, j, cp, lp, c);
            else:
                cp, lp = solve_tile(i - 1, j - 1, cp, lp, c);
                cp, lp = solve_tile(i - 1, j, cp, lp, c);
                cp, lp = solve_tile(i - 1, j + 1, cp, lp, c);
                cp, lp = solve_tile(i, j + 1, cp, lp, c);
                cp, lp = solve_tile(i + 1, j + 1, cp, lp, c);
                cp, lp = solve_tile(i + 1, j, cp, lp, c);
                cp, lp = solve_tile(i + 1, j - 1, cp, lp, c);
                cp, lp = solve_tile(i, j - 1, cp, lp, c);
#        print("Backtracking")
        cp.pop();
        board[i][j].visited = False;
        return cp, lp

    elif board[i][j].type == "2":
#        print("type 2 entered");
        if i == 0 or i == 1:                            # If top 2 rows
            if j == 0 or j == 1:                            # If first 2 columns
                cp, lp = solve_tile(i, j + 2, cp, lp, c);
                cp, lp = solve_tile(i + 2, j + 2, cp, lp, c);
                cp, lp = solve_tile(i + 2, j, cp, lp, c);
            elif j == DIM - 1 or j == DIM - 2:  # If last 2 columns
                cp, lp = solve_tile(i + 2, j, cp, lp, c);
                cp, lp = solve_tile(i + 2, j - 2, cp, lp, c);
                cp, lp = solve_tile(i, j - 2, cp, lp, c);
            else:
                cp, lp = solve_tile(i, j + 2, cp, lp, c);
                cp, lp = solve_tile(i + 2, j + 2, cp, lp, c);
                cp, lp = solve_tile(i + 2, j, cp, lp, c);
                cp, lp = solve_tile(i + 2, j - 2, cp, lp, c);
                cp, lp = solve_tile(i, j - 2, cp, lp, c);
        elif i == DIM - 1 or i == DIM - 2:  # If bottom 2 rows
            if j == 0 or j == 1:                            # If first 2 columns
                cp, lp = solve_tile(i - 2, j, cp, lp, c);
                cp, lp = solve_tile(i - 2, j + 2, cp, lp, c);
                cp, lp = solve_tile(i, j + 2, cp, lp, c);
            elif j == DIM - 1 or j == DIM - 2:  # If last 2 columns
                cp, lp = solve_tile(i, j - 2, cp, lp, c);
                cp, lp = solve_tile(i - 2, j - 2, cp, lp, c);
                cp, lp = solve_tile(i - 2, j, cp, lp, c);
            else:                                           # If middle columns
                cp, lp = solve_tile(i, j - 2, cp, lp, c);
                cp, lp = solve_tile(i - 2, j - 2, cp, lp, c);
                cp, lp = solve_tile(i - 2, j, cp, lp, c);
                cp, lp = solve_tile(i - 2, j + 2, cp, lp, c);
                cp, lp = solve_tile(i, j + 2, cp, lp, c);
        else:                                           # If middle rows
            if j == 0 or j == 1:
                cp, lp = solve_tile(i - 2, j, cp, lp, c);
                cp, lp = solve_tile(i - 2, j + 2, cp, lp, c);
                cp, lp = solve_tile(i, j + 2, cp, lp, c);
                cp, lp = solve_tile(i + 2, j + 2, cp, lp, c);
                cp, lp = solve_tile(i + 2, j, cp, lp, c);
            elif j == DIM - 1 or j == DIM - 2:
                cp, lp = solve_tile(i + 2, j, cp, lp, c);
                cp, lp = solve_tile(i + 2, j - 2, cp, lp, c);
                cp, lp = solve_tile(i, j - 2, cp, lp, c);
                cp, lp = solve_tile(i - 2, j - 2, cp, lp, c);
                cp, lp = solve_tile(i - 2, j, cp, lp, c);
            else:
                cp, lp = solve_tile(i - 2, j, cp, lp, c);
                cp, lp = solve_tile(i - 2, j + 2, cp, lp, c);
                cp, lp = solve_tile(i, j + 2, cp, lp, c);
                cp, lp = solve_tile(i + 2, j + 2, cp, lp, c);
                cp, lp = solve_tile(i + 2, j, cp, lp, c);
                cp, lp = solve_tile(i + 2, j - 2, cp, lp, c);
                cp, lp = solve_tile(i, j - 2, cp, lp, c);
                cp, lp = solve_tile(i - 2, j - 2, cp, lp, c);
        #print("Backtracking");
        cp.pop();
        board[i][j].visited = False;
        return cp, lp;
    elif board[i][j].type == "3":
#        print("type 3 entered");
        if i == 0 or i == 1 or i == 2:
            if j == 0 or j == 1 or j == 2:
                cp, lp = solve_tile(i, j + 3, cp, lp, c);
                cp, lp = solve_tile(i + 3, j + 3, cp, lp, c);
                cp, lp = solve_tile(i + 3, j, cp, lp, c);
            elif j == DIM - 1 or j == DIM - 2 or j == DIM - 3:
                cp, lp = solve_tile(i + 3, j, cp, lp, c);
                cp, lp = solve_tile(i + 3, j - 3, cp, lp, c);
                cp, lp = solve_tile(i, j - 3, cp, lp, c);
            else:
                pass;
        elif i == DIM - 1 or i == DIM - 2 or i == DIM - 3:
            if j == 0  or j == 1 or j == 2:
                cp, lp = solve_tile(i - 3, j, cp, lp, c);
                cp, lp = solve_tile(i - 3, j + 3, cp, lp, c);
                cp, lp = solve_tile(i, j + 3, cp, lp, c);
            elif j == DIM - 1 or j == DIM - 2 or j == DIM - 3:
                cp, lp = solve_tile(i, j - 3, cp, lp, c);
                cp, lp = solve_tile(i - 3, j - 3, cp, lp, c);
                cp, lp = solve_tile(i - 3, j, cp, lp, c);
            else:
                pass;
        else:
            if j == 0 or j == 1 or j == 2:
                cp, lp = solve_tile(i - 3, j, cp, lp, c);
                cp, lp = solve_tile(i - 3, j + 3, cp, lp, c);
                cp, lp = solve_tile(i, j + 3, cp, lp, c);
                cp, lp = solve_tile(i + 3, j + 3, cp, lp, c);
                cp, lp = solve_tile(i + 3, j, cp, lp, c);
            elif j == DIM - 1 or j == DIM - 2 or j == DIM - 3:
                cp, lp = solve_tile(i + 3, j, cp, lp, c);
                cp, lp = solve_tile(i + 3, j - 3, cp, lp, c);
                cp, lp = solve_tile(i, j - 3, cp, lp, c);
                cp, lp = solve_tile(i - 3, j - 3, cp, lp, c);
                cp, lp = solve_tile(i - 3, j, cp, lp, c);
            else:
                cp, lp = solve_tile(i - 3, j, cp, lp, c);
                cp, lp = solve_tile(i - 3, j + 3, cp, lp, c);
                cp, lp = solve_tile(i, j + 3, cp, lp, c);
                cp, lp = solve_tile(i + 3, j + 3, cp, lp, c);
                cp, lp = solve_tile(i + 3, j, cp, lp, c);
                cp, lp = solve_tile(i + 3, j - 3, cp, lp, c);
                cp, lp = solve_tile(i, j - 3, cp, lp, c);
                cp, lp = solve_tile(i - 3, j - 3, cp, lp, c);
        #print("Backtracking");
        cp.pop();
        board[i][j].visited = False;
        return cp, lp;
    elif board[i][j].type == "4":
#        print("type 4 entered");
        if i < 2:
            if j < 2:                                       # Top left corner
                cp, lp = solve_tile(i, j + 4, cp, lp, c);
                cp, lp = solve_tile(i + 4, j + 4, cp, lp, c);
                cp, lp = solve_tile(i + 4, j, cp, lp, c);
            elif j ==  DIM - 1 or j == DIM - 2:             # Top right corner
                cp, lp = solve_tile(i + 4, j, cp, lp, c);
                cp, lp = solve_tile(i + 4, j - 4, cp, lp, c);
                cp, lp = solve_tile(i, j - 4, cp, lp, c);
            else:
                cp, lp = solve_tile(i + 4, j, cp, lp, c);
        elif i == DIM - 1 or i == DIM - 2:
            if j < 2:                                       # Bottom left corner
                cp, lp = solve_tile(i - 4, j, cp, lp, c);
                cp, lp = solve_tile(i - 4, j + 4, cp, lp, c);
                cp, lp = solve_tile(i, j + 4, cp, lp, c);
            elif j == DIM - 1 or j == DIM - 2:              # Bottom right corner
                cp, lp = solve_tile(i, j - 4, cp, lp, c);
                cp, lp = solve_tile(i - 4, j - 4, cp, lp, c);
                cp, lp = solve_tile(i, j - 4, cp, lp, c);
            else:
                cp, lp = solve_tile(i - 4, j, cp, lp, c);
        else:
            if j < 2:
                cp, lp = solve_tile(i, j + 4, cp, lp, c);
            elif j == DIM - 1 or j == DIM - 2:
                cp, lp = solve_tile(i, j - 4, cp, lp, c);
            else:
                pass;
        #print("Backtracking");
        cp.pop();
        board[i][j].visited = False;
        return cp, lp;
    elif board[i][j].type == "k":
#        print("type k enetered");
        if i == 0:
            if j == 0:
                cp, lp = solve_tile(i + 1, j + 2, cp, lp, c);
                cp, lp = solve_tile(i + 2, j + 1, cp, lp, c);
            elif j == 1:
                cp, lp = solve_tile(i + 1, j + 2, cp, lp, c);
                cp, lp = solve_tile(i + 2, j + 1, cp, lp, c);
                cp, lp = solve_tile(i + 2, j - 1, cp, lp, c);
            elif j == DIM - 1:
                cp, lp = solve_tile(i + 2, j - 1, cp, lp, c);
                cp, lp = solve_tile(i + 1, j - 2, cp, lp, c);
            elif j == DIM - 2:
                cp, lp = solve_tile(i + 2, j + 1, cp, lp, c);
                cp, lp = solve_tile(i + 2, j - 1, cp, lp, c);
                cp, lp = solve_tile(i + 1, j - 2, cp, lp, c);
            else:
                cp, lp = solve_tile(i + 1, j + 2, cp, lp, c);
                cp, lp = solve_tile(i + 2, j + 1, cp, lp, c);
                cp, lp = solve_tile(i + 2, j - 1, cp, lp, c);
                cp, lp = solve_tile(i + 1, j - 2, cp, lp, c);
        elif i == 1:
            if j == 0:
                cp, lp = solve_tile(i - 1, j + 2, cp, lp, c);
                cp, lp = solve_tile(i + 1, j + 2, cp, lp, c);
                cp, lp = solve_tile(i + 2, j + 1, cp, lp, c);
            elif j == 1:
                cp, lp = solve_tile(i - 1, j + 2, cp, lp, c);
                cp, lp = solve_tile(i + 1, j + 2, cp, lp, c);
                cp, lp = solve_tile(i + 2, j + 1, cp, lp, c);
                cp, lp = solve_tile(i + 2, j - 1, cp, lp, c);
            elif j == DIM - 1:
                cp, lp = solve_tile(i + 2, j - 1, cp, lp, c);
                cp, lp = solve_tile(i + 1, j - 2, cp, lp, c);
                cp, lp = solve_tile(i - 1, j - 2, cp, lp, c);
            elif j == DIM - 2:
                cp, lp = solve_tile(i + 2, j + 1, cp, lp, c);
                cp, lp = solve_tile(i + 2, j - 1, cp, lp, c);
                cp, lp = solve_tile(i + 1, j - 2, cp, lp, c);
                cp, lp = solve_tile(i - 1, j - 2, cp, lp, c);
            else:
                cp, lp = solve_tile(i + 1, j + 2, cp, lp, c);
                cp, lp = solve_tile(i + 2, j + 1, cp, lp, c);
                cp, lp = solve_tile(i + 1, j - 1, cp, lp, c);
                cp, lp = solve_tile(i + 1, j - 2, cp, lp, c);
                cp, lp = solve_tile(i - 1, j - 2, cp, lp, c);
        elif i == DIM - 1:
            if j == 0:
                cp, lp = solve_tile(i - 2, j + 1, cp, lp, c);
                cp, lp = solve_tile(i - 1, j + 2, cp, lp, c);
            elif j == 1:
                cp, lp = solve_tile(i - 2, j - 1, cp, lp, c);
                cp, lp = solve_tile(i - 2, j + 1, cp, lp, c);
                cp, lp = solve_tile(i - 1, j + 2, cp, lp, c);
            elif j == DIM - 1:
                cp, lp = solve_tile(i - 1, j - 2, cp, lp, c);
                cp, lp = solve_tile(i - 2, j - 1, cp, lp, c);
            elif j == DIM - 2:
                cp, lp = solve_tile(i - 1, j - 2, cp, lp, c);
                cp, lp = solve_tile(i - 2, j - 1, cp, lp, c);
                cp, lp = solve_tile(i - 2, j + 1, cp, lp, c);
            else:
                cp, lp = solve_tile(i - 1, j - 2, cp, lp, c);
                cp, lp = solve_tile(i - 2, j - 1, cp, lp, c);
                cp, lp = solve_tile(i - 2, j + 1, cp, lp, c);
                cp, lp = solve_tile(i - 1, j + 2, cp, lp, c);
        elif i == DIM - 2:
            if j == 0:
                cp, lp = solve_tile(i - 2, j + 1, cp, lp, c);
                cp, lp = solve_tile(i - 1, j + 2, cp, lp, c);
                cp, lp = solve_tile(i + 1, j + 2, cp, lp, c);
            elif j == 1:
                cp, lp = solve_tile(i - 2, j - 1, cp, lp, c);
                cp, lp = solve_tile(i - 2, j + 1, cp, lp, c);
                cp, lp = solve_tile(i - 1, j + 2, cp, lp, c);
                cp, lp = solve_tile(i + 1, j + 2, cp, lp, c);
            elif j == DIM - 1:
                cp, lp = solve_tile(i + 1, j - 2, cp, lp, c);
                cp, lp = solve_tile(i - 1, j - 2, cp, lp, c);
                cp, lp = solve_tile(i - 2, j - 1, cp, lp, c);
            elif j == DIM - 2:
                cp, lp = solve_tile(i + 1, j - 2, cp, lp, c);
                cp, lp = solve_tile(i - 1, j - 2, cp, lp, c);
                cp, lp = solve_tile(i - 2, j - 1, cp, lp, c);
                cp, lp = solve_tile(i - 2, j + 1, cp, lp, c);
            else:
                cp, lp = solve_tile(i + 1, j - 2, cp, lp, c);
                cp, lp = solve_tile(i - 1, j - 2, cp, lp, c);
                cp, lp = solve_tile(i - 2, j - 1, cp, lp, c);
                cp, lp = solve_tile(i - 2, j + 1, cp, lp, c);
                cp, lp = solve_tile(i - 1, j + 2, cp, lp, c);
        else:
            if j == 0:
                cp, lp = solve_tile(i - 2, j + 1, cp, lp, c);
                cp, lp = solve_tile(i - 1, j + 2, cp, lp, c);
                cp, lp = solve_tile(i + 1, j + 2, cp, lp, c);
                cp, lp = solve_tile(i + 2, j + 1, cp, lp, c);
            elif j == 1:
                cp, lp = solve_tile(i - 2, j - 1, cp, lp, c);
                cp, lp = solve_tile(i - 2, j + 1, cp, lp, c);
                cp, lp = solve_tile(i - 1, j + 2, cp, lp, c);
                cp, lp = solve_tile(i + 1, j + 2, cp, lp, c);
                cp, lp = solve_tile(i + 2, j + 1, cp, lp, c);
                cp, lp = solve_tile(i + 2, j - 1, cp, lp, c);
            elif j == DIM - 1:
                cp, lp = solve_tile(i + 2, j - 1, cp, lp, c);
                cp, lp = solve_tile(i + 1, j - 2, cp, lp, c);
                cp, lp = solve_tile(i - 1, j - 2, cp, lp, c);
                cp, lp = solve_tile(i - 2, j - 1, cp, lp, c);
            elif j == DIM - 2:
                cp, lp = solve_tile(i + 2, j + 1, cp, lp, c);
                cp, lp = solve_tile(i + 2, j - 1, cp, lp, c);
                cp, lp = solve_tile(i + 1, j - 2, cp, lp, c);
                cp, lp = solve_tile(i - 1, j - 2, cp, lp, c);
                cp, lp = solve_tile(i - 2, j - 1, cp, lp, c);
                cp, lp = solve_tile(i - 2, j + 1, cp, lp, c);
            else:
                cp, lp = solve_tile(i - 2, j + 1, cp, lp, c);
                cp, lp = solve_tile(i - 1, j + 2, cp, lp, c);
                cp, lp = solve_tile(i + 1, j + 2, cp, lp, c);
                cp, lp = solve_tile(i + 2, j + 1, cp, lp, c);
                cp, lp = solve_tile(i + 2, j - 1, cp, lp, c);
                cp, lp = solve_tile(i + 1, j - 2, cp, lp, c);
                cp, lp = solve_tile(i - 1, j - 2, cp, lp, c);
                cp, lp = solve_tile(i - 2, j - 1, cp, lp, c);
        #print("Backtracking");
        cp.pop();
        board[i][j].visited = False;
        return cp, lp;
    elif board[i][j].type == 'r':
#        print("type r entered")
        if i == 0:
            if j == 0:
                cp, lp = solve_tile(i, DIM - 1, cp, lp, c);
                cp, lp = solve_tile(DIM - 1, j, cp, lp, c);
            elif j == DIM - 1:
                cp, lp = solve_tile(DIM - 1, j, cp, lp, c);
                cp, lp = solve_tile(i, 0, cp, lp, c);
            else:
                cp, lp = solve_tile(i, DIM - 1, cp, lp, c);
                cp, lp = solve_tile(DIM - 1, j, cp, lp, c);
                cp, lp = solve_tile(i, 0, cp, lp, c);
        elif i == DIM - 1:
            if j == 0:
                cp, lp = solve_tile(0, j, cp, lp, c);
                cp, lp = solve_tile(i, DIM - 1, cp, lp, c);
            elif j == DIM - 1:
                cp, lp = solve_tile(i, 0, cp, lp, c);
                cp, lp = solve_tile(0, j, cp, lp, c);
            else:
                cp, lp = solve_tile(i, 0, cp, lp, c);
                cp, lp = solve_tile(0, j, cp, lp, c);
                cp, lp = solve_tile(i, DIM - 1, cp, lp, c);
        else:
            if j == 0:
                cp, lp = solve_tile(0, j, cp, lp, c);
                cp, lp = solve_tile(i, DIM - 1, cp, lp, c);
                cp, lp = solve_tile(DIM - 1, j, cp, lp, c);
            elif j == DIM - 1:
                cp, lp = solve_tile(DIM - 1, j, cp, lp, c);
                cp, lp = solve_tile(i, 0, cp, lp, c);
                cp, lp = solve_tile(0, j, cp, lp, c);
            else:
                cp, lp = solve_tile(0, j, cp, lp, c);
                cp, lp = solve_tile(i, DIM - 1, cp, lp, c);
                cp, lp = solve_tile(DIM - 1, j, cp, lp, c);
                cp, lp = solve_tile(i, 0, cp, lp, c);
        #print("Backtracking");
        cp.pop();
        board[i][j].visited = False;
        return cp, lp;
    elif board[i][j].type == 'b':
#        print("type b entered");
        corners = find_corners(i, j, DIM);
        #print("corners:");
        #print(corners);
        for corner in corners:
            #print(corner);
            (x, y) = corner;
            #print("x: %d" %x);
            #print("y: %d" %y)
            cp, lp = solve_tile(x, y, cp, lp, c);
        #print("Backtracking");
        cp.pop();
        board[i][j].visited = False;
        return cp, lp;
    elif board[i][j].type == 'q':
#        print("type q entered");
        sides = find_sides(i, j, DIM);
        corners = find_corners(i, j, DIM);
        for side in sides:
            (x, y) = side;
            cp, lp = solve_tile(x, y, cp, lp, c);
        for corner in corners:
            (x, y) = corner;
            cp, lp = solve_tile(x, y, cp, lp, c);
        #print("Backtracking");
        cp.pop();
        board[i][j].visited = False;
        return cp, lp;
    else:
        print("Error: unknown type");
        print("Aborting!");
        sys.exit(0);


##### START OF PROGRAM ############

menu_opt = 0;

while(menu_opt != 9):

    board = np.ndarray(shape=(DIM, DIM), dtype=Tile);

    cur_path = [];
    long_path = [];

    entry_tiles = [];
    longest = [];



    if platform.system()=="Windows":
        subprocess.Popen("cls", shell=True).communicate() #I like to use this instead of subprocess.call since for multi-word commands you can just type it out, granted this is just cls and subprocess.call should work fine 
    else: #Linux and Mac
        print("\033c", end="")

    print("\nYag's Lazy Blacksmithing Solution\n");

    print("Input board tiles (from left to right for each row):");

    for i in range(DIM):
        for j in range(DIM):
            type = input("Enter tile (%d, %d): " %(i, j));
            board[i][j] = Tile(type, False);

    entry_count = input("\nEnter amount of starting tiles (3 will do for 1st layer): ");
    print("Enter starting tile coordinates:");
    for c in range(int(entry_count)):
        x, y = input("Entry tile: ").split()
        entry_tiles.append((int(x), int(y)));

    sol_count = 1
    for sol in entry_tiles:
#        print("ATTEMPT %d" %(sol_count));
#        input("");
#        sol_count += 1
        (i, j) = sol;
        r_count = time.perf_counter();
        cur_path, long_path = solve_tile(i, j, cur_path, long_path, r_count);
        #r_count = time.perf_counter();
        if(len(long_path) == 36):
            longest = copy.deepcopy(long_path);
            break;
        else:
            if(len(long_path) > len(longest)):
                longest = copy.deepcopy(long_path);

    if platform.system()=="Windows":
        subprocess.Popen("cls", shell=True).communicate() #I like to use this instead of subprocess.call since for multi-word commands you can just type it out, granted this is just cls and subprocess.call should work fine 
    else: #Linux and Mac
        print("\033c", end="")

    print("\nYag's Lazy Blacksmithing Solution\n");

    print("\nLongest path found within time-limit: %d/36" %len(longest));
    if(len(longest) == 36):
        print("Full path found!");
    else:
        print("Full path NOT found.");

    print("SOLUTION:");
    count = 1

    letters = {0: "A", 1: "B", 2: "C", 3: "D", 4: "E", 5: "F"};
    for move in longest:
        row, col = move;
        print("Step %d: %s%d" %(count, letters[col], row + 1));
        count += 1
    print("\n")

    while(True):
        inp = input("Continue? y/n: ");
        if inp == "y":
            break;
        elif inp == "n":
            menu_opt = 9;
            break;
        else:
            print("Enter 'y' for yes and 'n' for no");
